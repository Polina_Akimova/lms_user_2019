// Sidebar Routers
export const menus = {
	
	'message.modules': [
		{
			action: 'zmdi-accounts',
			title: 'message.profile',
			active: false,
			items: null,
			path: '/profile',
			label: 'Old',
			role: [2],
			exact: true
		},
		{
			action: 'zmdi-book',
			title: 'message.courses',
			active: false,
			items: null,
			path: '/courses',
			label: 'Old',
			role: [2],
			exact: true
		},
		{
			action: 'zmdi-coffee',
			title: 'message.projects',
			active: false,
			items: null,
			path: '/projects',
			label: 'Old',
			role: [2],
			exact: true
		}
	],
	
}
