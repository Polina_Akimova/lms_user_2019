/**
 * Auth Module
 */
import Vue from 'vue'

import Nprogress from 'nprogress';
import router from '../../../router';
import API from 'Api'

const state = {
    user: localStorage.getItem('user'),
    role: localStorage.getItem('role'),
}

// getters
const getters = {
    getUser: state => {
        return state.user;
    },
    getRole: state => {
        return state.role
    }
}

// actions
const actions = {
    signinUser(context, user) {
        context.commit('loginUser');
        const data = new FormData();
        data.append('mail', user.email);
        data.append('pass', user.password);

        API
        .post('user/login', data)
        .then(function(response){
            if (response.data.error === 0){
                const user = {
                    token: response.data.token,
                    role: parseInt(response.data.role)
                }
                context.commit('loginUserSuccess', user);
                
            }else if (response.data.error === 5){
                const error = {
                    message:'Активируйте почту'
                }
                context.commit('loginUserFailure', error);
            }else{
                const error = {
                    message:'Неправильные данные'
                }
                context.commit('loginUserFailure', error);
            }
        });
    },
    setRole(context, role){
        context.commit('setRole', role);
    },
    logout(context)
    {
        context.commit('logoutUser');
    }
}

// mutations
const mutations = {
    loginUser(state) {
        Nprogress.start();
    },
    loginUserSuccess(state, user) {
        state.user = user.token;
        state.role = user.role;
        localStorage.setItem('user', user.token);
        localStorage.setItem('role', JSON.stringify(user.role));

        router.push("/profile");
        setTimeout(function(){
            Vue.notify({
                group: 'loggedIn',
                type: 'success',
                text: 'User Logged In Success!'
            });
       },1500);
    },
    loginUserFailure(state, error) {
        Nprogress.done();
        Vue.notify({
            group: 'loggedIn',
            type: 'error',
            text: error.message
        });
    },
    setRole(state, role) {
        state.role = role;
        localStorage.setItem('role', role);
    },
    logoutUser(state) {
        state.user = null
        state.role = null

        localStorage.removeItem('user');
        localStorage.removeItem('role');
        router.push("/session/login");
    },
    signUpUser(state) {
        Nprogress.start();
    },
    signUpUserSuccess(state, user) {
        state.user = localStorage.setItem('user', user);
        router.push("/default/dashboard/ecommerce");
        Vue.notify({
            group: 'loggedIn',
            type: 'success',
            text: 'Account Created!'
        });
    },
    signUpUserFailure(state, error) {
        Nprogress.done();
         Vue.notify({
            group: 'loggedIn',
            type: 'error',
            text: error.message
        });
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
