import Full from 'Container/Full'
import CourseDetail1 from "../views/courses/CourseDetail1";
//my views

const register = () => import('Views/my/register')
const NewUser = () => import('Views/my/new');
const CourseList = () => import('Views/courses/CourseList');
const CourseDetail = () => import('Views/courses/CoursesDetail');

const Profile = () => import('Views/my/profile');
const Projects = () => import('Views/my/Projects1');
const ProjectDetail = () => import("Views/my/ProjectWidgets/ProjectDetail");
const editProfile = () => import('Views/my/editProfile');
const Certificate = () => import('Views/my/Certificate');
export default {
   path: '/',
   component: Full,
   redirect: '/profile',
   children: [
      {
         path: '/profile',
         component: Profile,
         meta: {
           requiresAuth: true,
           title: 'message.profile',
           breadcrumb: null
        }
     },
     
      {
        path: '/courses',
        component: CourseList,
        meta: {
          requiresAuth: true,
          title: 'message.courses',
          breadcrumb: null
       }
      },
      {
         
         path: '/course/details/:id',
         component: CourseDetail1,
         meta: {
            requiresAuth: true,
            title: 'message.courses',
            breadcrumb: null
         }
      },
      {
        path: '/projects',
        component: Projects,
        meta: {
          requiresAuth: true,
          title: 'message.projects',
          breadcrumb: null
       }
      },
       {
           path: '/project/detail/:id',
           component: ProjectDetail,
           meta: {
               requiresAuth: true,
               title: 'message.projects',
               breadcrumb: null
           }
       },
      {
         path: '/profile/edit',
         component: editProfile,
         meta: {
           requiresAuth: true,
           title: 'message.profileEdit',
           breadcrumb: null
        }
       },
       {
           path: '/certificate',
           component: Certificate,
           meta: {
               requiresAuth: true,
               title: 'message.certificate',
               breadcrumb: null
           }
       },

   ]
}
